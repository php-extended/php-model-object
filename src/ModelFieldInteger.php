<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Internationalizable\InternationalizableStatusNoOnly;
use PhpExtended\Optionality\OptionalityInterface;

/**
 * ModelFieldInteger class file.
 * 
 * This class represents an integer field in a relational database.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ModelFieldInteger extends AbstractModelField implements ModelFieldIntegerInterface
{
	
	/**
	 * Whether this integer is signed.
	 * 
	 * @var boolean
	 */
	protected bool $_signed = true;
	
	/**
	 * The capacity of the integer, in bits.
	 * 
	 * @var IntegerCapacityInterface
	 */
	protected IntegerCapacityInterface $_capacity;
	
	/**
	 * The default integer value for this field.
	 * 
	 * @var ?integer
	 */
	protected ?int $_defaultValue;
	
	/**
	 * Builds a new ModelFieldInteger with the given field values.
	 * 
	 * @param string $name
	 * @param OptionalityInterface $optionality
	 * @param boolean $signed
	 * @param ?IntegerCapacityInterface $capacity
	 * @param ?InternationalizableStatusInterface $status
	 * @param ?integer $default
	 * @param ?string $comment
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public function __construct(
		string $name,
		OptionalityInterface $optionality,
		bool $signed = true,
		?IntegerCapacityInterface $capacity = null,
		?InternationalizableStatusInterface $status = null,
		?int $default = null,
		?string $comment = null
	) {
		if(null === $capacity)
		{
			$capacity = new BitIntegerCapacity(32);
		}
		if(null === $status)
		{
			$status = new InternationalizableStatusNoOnly();
		}
		parent::__construct($name, $optionality, $status, $comment);
		$this->_signed = $signed;
		$this->_capacity = $capacity;
		$this->_defaultValue = $default;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'('.($this->_signed ? '' : 'u').((string) $this->_capacity->getNumberOfBits()).')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldIntegerInterface::isSigned()
	 */
	public function isSigned() : bool
	{
		return $this->_signed;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldIntegerInterface::getCapacity()
	 */
	public function getCapacity() : IntegerCapacityInterface
	{
		return $this->_capacity;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldIntegerInterface::getDefaultValue()
	 */
	public function getDefaultValue() : ?int
	{
		return $this->_defaultValue;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::mergeWith()
	 */
	public function mergeWith(ModelFieldInterface $field) : ModelFieldInterface
	{
		if(\is_subclass_of($field, ModelFieldFloatInterface::class)
			|| \is_subclass_of($field, ModelFieldSpatialInterface::class)
			|| \is_subclass_of($field, ModelFieldBlobInterface::class)
			|| \is_subclass_of($field, ModelFieldStringInterface::class)
			|| \is_subclass_of($field, ModelFieldDateTimeInterface::class)
		) {
			return $field->mergeWith($this);
		}
		
		$isSigned = $this->isSigned();
		$capacity = $this->getCapacity();
		
		if(\is_subclass_of($field, ModelFieldIntegerInterface::class))
		{
			$isSigned = $isSigned || $field->isSigned();
			$capacity = $capacity->mergeWith($field->getCapacity());
		}
		
		return new self(
			$this->getName(),
			$this->getOptionality()->mergeWith($field->getOptionality()),
			$isSigned,
			$capacity,
			$this->getInternationalizedStatus()->mergeWith($field->getInternationalizedStatus()),
			$this->getDefaultValue(),
			$this->getComment(),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::visit()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visit(ModelFieldVisitorInterface $visitor)
	{
		return $visitor->visitIntegerField($this);
	}
	
}
