<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use DateTimeInterface;
use PhpExtended\DateTimeFormat\DateTimeFormat;
use PhpExtended\DateTimeFormat\DateTimeFormatInterface;
use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Internationalizable\InternationalizableStatusNoOnly;
use PhpExtended\Optionality\OptionalityInterface;

/**
 * ModelFieldDateTime class file.
 * 
 * This class represents a date and/or time field in a relational database.
 * 
 * @author Anastaszor
 */
class ModelFieldDateTime extends AbstractModelField implements ModelFieldDateTimeInterface
{
	
	/**
	 * The format to store the date and/or time data in the field.
	 * 
	 * @var DateTimeFormatInterface
	 */
	protected DateTimeFormatInterface $_format;
	
	/**
	 * Whether this field is automatically updated on create of the record.
	 * 
	 * @var boolean
	 */
	protected bool $_isInitdOnCreate = false;
	
	/**
	 * Whether this field is automatically updated on update of the record.
	 * 
	 * @var boolean
	 */
	protected bool $_isModifiedOnUpdate = false;
	
	/**
	 * The default datetime value for this field.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_defaultValue = null;
	
	/**
	 * Builds a new ModelFieldDateTime object with the given field values.
	 * 
	 * @param string $name
	 * @param OptionalityInterface $optionality
	 * @param ?DateTimeFormatInterface $format
	 * @param ?InternationalizableStatusInterface $status
	 * @param boolean $isInitdOnCreate
	 * @param boolean $isModifiedOnUpdate
	 * @param ?DateTimeInterface $default
	 * @param ?string $comment
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public function __construct(
		string $name,
		OptionalityInterface $optionality,
		?DateTimeFormatInterface $format = null,
		?InternationalizableStatusInterface $status = null,
		bool $isInitdOnCreate = false,
		bool $isModifiedOnUpdate = false,
		?DateTimeInterface $default = null,
		?string $comment = null
	) {
		if(null === $format)
		{
			$format = DateTimeFormat::DATETIME;
		}
		if(null === $status)
		{
			$status = new InternationalizableStatusNoOnly();
		}
		parent::__construct($name, $optionality, $status, $comment);
		$this->_format = $format;
		$this->_isInitdOnCreate = $isInitdOnCreate;
		$this->_isModifiedOnUpdate = $isModifiedOnUpdate;
		$this->_defaultValue = $default;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'['.$this->_format->getPhpDateTimeFormat().']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldDateTimeInterface::getFormat()
	 */
	public function getFormat() : DateTimeFormatInterface
	{
		return $this->_format;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldDateTimeInterface::isInitializedOnCreate()
	 */
	public function isInitializedOnCreate() : bool
	{
		return $this->_isInitdOnCreate;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldDateTimeInterface::isModifiedOnUpdate()
	 */
	public function isModifiedOnUpdate() : bool
	{
		return $this->_isModifiedOnUpdate;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldDateTimeInterface::getDefaultValue()
	 */
	public function getDefaultValue() : ?DateTimeInterface
	{
		return $this->_defaultValue;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::mergeWith()
	 */
	public function mergeWith(ModelFieldInterface $field) : ModelFieldInterface
	{
		if($field instanceof ModelFieldSpatialInterface
			|| $field instanceof ModelFieldBlobInterface
			|| $field instanceof ModelFieldStringInterface
		) {
			return $field->mergeWith($this);
		}
		
		$isInitdOnCreate = $this->isInitializedOnCreate();
		$isModifiedOnUpdate = $this->isModifiedOnUpdate();
		
		if($field instanceof ModelFieldDateTimeInterface)
		{
			$isInitdOnCreate = $isInitdOnCreate || $field->isInitializedOnCreate();
			$isModifiedOnUpdate = $isModifiedOnUpdate || $field->isModifiedOnUpdate();
		}
		
		return new self(
			$this->getName(),
			$this->getOptionality()->mergeWith($field->getOptionality()),
			$this->getFormat(),
			$this->getInternationalizedStatus()->mergeWith($field->getInternationalizedStatus()),
			$isInitdOnCreate,
			$isModifiedOnUpdate,
			$this->getDefaultValue(),
			$this->getComment(),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::visit()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visit(ModelFieldVisitorInterface $visitor)
	{
		return $visitor->visitDateTimeField($this);
	}
	
}
