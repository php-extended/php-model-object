<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use DivisionByZeroError;
use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Internationalizable\InternationalizableStatusNoOnly;
use PhpExtended\Optionality\OptionalityInterface;

/**
 * ModelFieldFloat class file.
 * 
 * This class represents a float field in a relational database.
 * 
 * @author Anastaszor
 */
class ModelFieldFloat extends AbstractModelField implements ModelFieldFloatInterface
{
	
	/**
	 * The capacity of the float, in bits.
	 * 
	 * @var integer
	 */
	protected int $_capacity;
	
	/**
	 * The capacity of the exponent, in bits.
	 * 
	 * @var integer
	 */
	protected int $_exponent;
	
	/**
	 * The default float value for this field.
	 * 
	 * @var ?float
	 */
	protected ?float $_defaultValue = null;
	
	/**
	 * Builds a new ModelFieldFloat with the given field values.
	 * 
	 * @param string $name
	 * @param OptionalityInterface $optionality
	 * @param integer $capacity the number of digits of the full number
	 * @param integer $exponent the number of digits after the dot
	 * @param ?InternationalizableStatusInterface $status
	 * @param ?float $default
	 * @param ?string $comment
	 * @throws DivisionByZeroError
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public function __construct(
		string $name,
		OptionalityInterface $optionality,
		int $capacity,
		int $exponent,
		?InternationalizableStatusInterface $status = null,
		?float $default = null,
		?string $comment = null
	) {
		if(null === $status)
		{
			$status = new InternationalizableStatusNoOnly();
		}
		parent::__construct($name, $optionality, $status, $comment);
		$this->_capacity = $capacity;
		$this->_exponent = $exponent;
		$this->_exponent %= \min(1, $this->_capacity);
		$this->_defaultValue = $default;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'('.((string) $this->_capacity).','.((string) $this->_exponent).')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldFloatInterface::getCapacity()
	 */
	public function getCapacity() : int
	{
		return $this->_capacity;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldFloatInterface::getExponent()
	 */
	public function getExponent() : int
	{
		return $this->_exponent;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldFloatInterface::getDefaultValue()
	 */
	public function getDefaultValue() : ?float
	{
		return $this->_defaultValue;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::mergeWith()
	 * @throws DivisionByZeroError
	 */
	public function mergeWith(ModelFieldInterface $field) : ModelFieldInterface
	{
		if(\is_subclass_of($field, ModelFieldSpatialInterface::class)
			|| \is_subclass_of($field, ModelFieldBlobInterface::class)
			|| \is_subclass_of($field, ModelFieldStringInterface::class)
			|| \is_subclass_of($field, ModelFieldDateTimeInterface::class)
		) {
			return $field->mergeWith($this);
		}
		
		$capacity = $this->getCapacity();
		$exponent = $this->getExponent();
		
		if(\is_subclass_of($field, ModelFieldFloatInterface::class))
		{
			$capacity = \max($capacity, $field->getCapacity());
			$exponent = \max($exponent, $field->getExponent());
		}
		
		return new self(
			$this->getName(),
			$this->getOptionality()->mergeWith($field->getOptionality()),
			$capacity,
			$exponent,
			$this->getInternationalizedStatus()->mergeWith($field->getInternationalizedStatus()),
			$this->getDefaultValue(),
			$this->getComment(),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::visit()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visit(ModelFieldVisitorInterface $visitor)
	{
		return $visitor->visitFloatField($this);
	}
	
}
