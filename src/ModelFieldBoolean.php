<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Internationalizable\InternationalizableStatusNoOnly;
use PhpExtended\Optionality\Optionality;

/**
 * ModelFieldBoolean class file.
 * 
 * This class represents a boolean field in a relational database.
 * 
 * @author Anastaszor
 */
class ModelFieldBoolean extends AbstractModelField implements ModelFieldBooleanInterface
{
	
	/**
	 * The default value of this boolean field.
	 * 
	 * @var boolean
	 */
	protected bool $_defaultValue = false;
	
	/**
	 * Builds a new ModelFieldBoolean with the given field values.
	 * 
	 * @param string $name
	 * @param ?InternationalizableStatusInterface $status
	 * @param boolean $default
	 * @param ?string $comment
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public function __construct(string $name, ?InternationalizableStatusInterface $status = null, bool $default = false, ?string $comment = null)
	{
		if(null === $status)
		{
			$status = new InternationalizableStatusNoOnly();
		}
		parent::__construct($name, Optionality::NO_NULL_EMPTY_ALLOWED, $status, $comment);
		$this->_defaultValue = $default;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::__toString()
	 */
	public function __toString() : string
	{
		return static::class;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldBooleanInterface::getDefaultValue()
	 */
	public function getDefaultValue() : bool
	{
		return $this->_defaultValue;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::mergeWith()
	 */
	public function mergeWith(ModelFieldInterface $field) : ModelFieldInterface
	{
		if(!$field instanceof ModelFieldBooleanInterface)
		{
			return $field->mergeWith($this);
		}
		
		return new self(
			$this->getName(),
			$this->getInternationalizedStatus()->mergeWith($field->getInternationalizedStatus()),
			$this->getDefaultValue(),
			$this->getComment(),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::visit()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visit(ModelFieldVisitorInterface $visitor)
	{
		return $visitor->visitBooleanField($this);
	}
	
}
