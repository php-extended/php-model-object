<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Internationalizable\InternationalizableStatusNoOnly;
use PhpExtended\Optionality\OptionalityInterface;

/**
 * ModelObject class file.
 * 
 * This class is a simple implementation of the ModelObjectInterface.
 * 
 * @author Anastaszor
 */
class ModelObject implements ModelObjectInterface
{
	
	/**
	 * Whether this object is enabled.
	 * 
	 * @var boolean
	 */
	protected bool $_isEnabled = true;
	
	/**
	 * Whether this object is internationalized.
	 * 
	 * @var boolean
	 */
	protected bool $_isInternationalized = false;
	
	/**
	 * The object name.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The comment for this model.
	 * 
	 * @var ?string
	 */
	protected ?string $_comment = null;
	
	/**
	 * The fields of this object.
	 * 
	 * @var array<integer, ModelFieldInterface>
	 */
	protected array $_fields = [];
	
	/**
	 * The names of the fields that are in indexes.
	 * 
	 * @var array<string, ModelObjectIndex>
	 */
	protected array $_indexes = [];
	
	/**
	 * The relations of this object.
	 * 
	 * @var array<integer, ModelRelationInterface>
	 */
	protected array $_relations = [];
	
	/**
	 * The relations to this object.
	 * 
	 * @var array<integer, ModelRelationInterface>
	 */
	protected array $_reverseRelations = [];
	
	/**
	 * Builds a new ModelObject with the given default parameters.
	 * 
	 * @param string $name
	 * @param boolean $enabled
	 * @param boolean $internationalized
	 * @param string $comment
	 */
	public function __construct(string $name, bool $enabled, bool $internationalized, ?string $comment = null)
	{
		$this->_name = $name;
		$this->_isEnabled = $enabled;
		$this->_isInternationalized = $internationalized;
		$this->_comment = $comment;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelObjectInterface::isEnabled()
	 */
	public function isEnabled() : bool
	{
		return $this->_isEnabled;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelObjectInterface::isInternationalized()
	 */
	public function isInternationalized() : bool
	{
		return $this->_isInternationalized;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelObjectInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelObjectInterface::getComment()
	 */
	public function getComment() : ?string
	{
		return $this->_comment;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelObjectInterface::getTableName()
	 */
	public function getTableName() : string
	{
		return (string) \mb_strtolower($this->_name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelObjectInterface::getI18nTableName()
	 */
	public function getI18nTableName() : string
	{
		return $this->getTableName().'_i18n';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelObjectInterface::getTablePk()
	 */
	public function getTablePk() : array
	{
		return [$this->getTableName().'_id'];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelObjectInterface::getI18nTablePk()
	 */
	public function getI18nTablePk() : array
	{
		return [$this->getI18nTableName().'_id'];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelObjectInterface::getTableComment()
	 */
	public function getTableComment() : string
	{
		if(null !== $this->_comment)
		{
			return $this->_comment;
		}
		
		return 'Table to store all the '.((string) \mb_strtolower((string) \preg_replace('#([A-Z])#', ' $1', $this->getTableName()))).' records';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelObjectInterface::getI18nTableComment()
	 */
	public function getI18nTableComment() : string
	{
		if(null !== $this->_comment)
		{
			return $this->_comment;
		}
		
		return 'Table to store all the '.((string) \mb_strtolower((string) \preg_replace('#([A-Z])#', ' $1', $this->getI18nTableName()))).' records.';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelObjectInterface::getFields()
	 */
	public function getFields() : array
	{
		return $this->_fields;
	}
	
	/**
	 * Adds a field to the data object.
	 * 
	 * @param ModelFieldInterface $field
	 * @param array<integer, string> $indexList the list of index names where this field will
	 *                                          also appear
	 */
	public function addField(ModelFieldInterface $field, array $indexList = []) : void
	{
		foreach($this->_fields as $ifield)
		{
			if($ifield->getName() === $field->getName())
			{
				return;
			}
		}
		
		$this->_fields[] = $field;
		
		foreach($indexList as $index)
		{
			if(!isset($this->_indexes[$index]))
			{
				$this->_indexes[$index] = new ModelObjectIndex($field->getName());
				
				continue;
			}
			
			$this->_indexes[$index]->addField($field->getName());
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelObjectInterface::getIndexes()
	 */
	public function getIndexes() : array
	{
		return \array_values($this->_indexes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelObjectInterface::getRelations()
	 */
	public function getRelations() : array
	{
		return $this->_relations;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelObjectInterface::getReverseRelations()
	 */
	public function getReverseRelations() : array
	{
		return $this->_reverseRelations;
	}
	
	/**
	 * Adds a belongs to relation to this model object. This means that this
	 * model object belongs to the other given model object through a relation
	 * with the given name. That relation may the source end (meaning this
	 * object) internationalizable, or the target end (meaning the given model
	 * object) internationalizable, and can carry multiple fields.
	 * 
	 * The reverse relation of a belongs to relation is an has many relation.
	 * 
	 * @param string $name
	 * @param OptionalityInterface $optionality
	 * @param ModelObjectInterface $otherObject
	 * @param InternationalizableStatusInterface $sourcei18n
	 * @param InternationalizableStatusInterface $targeti18n
	 * @param array<integer, ModelFieldInterface> $fields
	 * @param string $comment
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public function belongsTo(
		string $name,
		OptionalityInterface $optionality,
		ModelObjectInterface $otherObject,
		?InternationalizableStatusInterface $sourcei18n = null,
		?InternationalizableStatusInterface $targeti18n = null,
		array $fields = [],
		?string $comment = null
	) : void {
		if(null === $sourcei18n)
		{
			$sourcei18n = new InternationalizableStatusNoOnly();
		}
		if(null === $targeti18n)
		{
			$targeti18n = new InternationalizableStatusNoOnly();
		}
		$relation = new ModelRelationBelongsTo($name, $optionality, $this, $otherObject, $sourcei18n, $targeti18n, $fields, $comment);
		$this->addRelation($relation);
		/** @phpstan-ignore-next-line */ /** @psalm-suppress UndefinedInterfaceMethod */
		$otherObject->addReverseRelation($relation);
	}
	
	/**
	 * Adds a has many relation to this model object. This means that this
	 * model object may have many other given model objects through a relation
	 * with the given name. That relation may the source end (meaning this
	 * object) internationalizable, or the target end (meaning the given model
	 * object) internationalizable, and can carry multiple fields.
	 * 
	 * The reverse relation of an has many relation is a belongs to relation.
	 * 
	 * @param string $name
	 * @param OptionalityInterface $optionality
	 * @param ModelObjectInterface $otherObject
	 * @param InternationalizableStatusInterface $sourcei18n
	 * @param InternationalizableStatusInterface $targeti18n
	 * @param array<integer, ModelFieldInterface> $fields
	 * @param string $comment
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public function hasMany(
		string $name,
		OptionalityInterface $optionality,
		ModelObjectInterface $otherObject,
		?InternationalizableStatusInterface $sourcei18n = null,
		?InternationalizableStatusInterface $targeti18n = null,
		array $fields = [],
		?string $comment = null
	) : void {
		if(null === $sourcei18n)
		{
			$sourcei18n = new InternationalizableStatusNoOnly();
		}
		if(null === $targeti18n)
		{
			$targeti18n = new InternationalizableStatusNoOnly();
		}
		$relation = new ModelRelationHasMany($name, $optionality, $this, $otherObject, $sourcei18n, $targeti18n, $fields, $comment);
		$this->addRelation($relation);
		/** @phpstan-ignore-next-line */ /** @psalm-suppress UndefinedInterfaceMethod */
		$otherObject->addReverseRelation($relation);
	}
	
	/**
	 * Adds a many many relation to this model object. This means that this 
	 * model object may have many other given model objects, and that other 
	 * given model object may have many of this model object through a relation
	 * with the given name. That relation may the source end (meaning this
	 * object) internationalizable, or the target end (meaning the given model
	 * object) internationalizable, and can carry multiple fields.
	 * 
	 * @param string $name
	 * @param OptionalityInterface $optionality
	 * @param ModelObjectInterface $otherObject
	 * @param InternationalizableStatusInterface $sourcei18n
	 * @param InternationalizableStatusInterface $targeti18n
	 * @param array<integer, ModelFieldInterface> $fields
	 * @param string $comment
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public function manyMany(
		string $name,
		OptionalityInterface $optionality,
		ModelObjectInterface $otherObject,
		?InternationalizableStatusInterface $sourcei18n = null,
		?InternationalizableStatusInterface $targeti18n = null,
		array $fields = [],
		?string $comment = null
	) : void {
		if(null === $sourcei18n)
		{
			$sourcei18n = new InternationalizableStatusNoOnly();
		}
		if(null === $targeti18n)
		{
			$targeti18n = new InternationalizableStatusNoOnly();
		}
		$relation = new ModelRelationManyMany($name, $optionality, $this, $otherObject, $sourcei18n, $targeti18n, $fields, $comment);
		$this->addRelation($relation);
		/** @phpstan-ignore-next-line */ /** @psalm-suppress UndefinedInterfaceMethod */
		$otherObject->addReverseRelation($relation);
	}
	
	/**
	 * Adds a relation as direct relation to this model object.
	 * 
	 * @param ModelRelationInterface $relation
	 */
	public function addRelation(ModelRelationInterface $relation) : void
	{
		$this->_relations[] = $relation;
	}
	
	/**
	 * Adds a relation as reverse relation to this model object.
	 * 
	 * @param ModelRelationInterface $relation
	 */
	public function addReverseRelation(ModelRelationInterface $relation) : void
	{
		$this->_reverseRelations[] = $relation;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelObjectInterface::mergeWith()
	 */
	public function mergeWith(ModelObjectInterface $object) : ModelObjectInterface
	{
		$new = new self(
			$this->getName(),
			$this->isEnabled() || $object->isEnabled(),
			$this->isInternationalized() || $object->isInternationalized(),
			$this->getComment(),
		);
		
		foreach($this->getFields() as $field)
		{
			$new->addField($field, $this->getIndexList($object->getTableName()));
		}
		
		foreach($object->getFields() as $field)
		{
			$new->addField($field, $this->getIndexList($object->getTableName()));
		}
		
		foreach($this->getRelations() as $relation)
		{
			$new->addRelation($relation);
		}
		
		foreach($object->getRelations() as $relation)
		{
			$new->addRelation($relation);
		}
		
		foreach($this->getReverseRelations() as $relation)
		{
			$new->addReverseRelation($relation);
		}
		
		foreach($object->getReverseRelations() as $relation)
		{
			$new->addReverseRelation($relation);
		}
		
		return $new;
	}
	
	/**
	 * Gets the name of the indexes in which the given field name is present.
	 * 
	 * @param string $name
	 * @return array<integer, string>
	 */
	protected function getIndexList(string $name) : array
	{
		$indexList = [];
		
		foreach($this->_indexes as $indexName => $index)
		{
			foreach($index->getFields() as $fieldName)
			{
				if($fieldName === $name)
				{
					$indexList[] = (string) $indexName;
					continue 2;
				}
			}
		}
		
		return $indexList;
	}
	
}
