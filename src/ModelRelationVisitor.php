<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * ModelRelationVisitor class file.
 * 
 * This class is a noop relation visitor to be extended. 
 * 
 * @author Anastaszor
 */
class ModelRelationVisitor implements ModelRelationVisitorInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationVisitorInterface::visitBelongsToRelation()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitBelongsToRelation(ModelRelationBelongsToInterface $relation)
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationVisitorInterface::visitHasManyRelation()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitHasManyRelation(ModelRelationHasManyInterface $relation)
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationVisitorInterface::visitManyManyRelation()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitManyManyRelation(ModelRelationManyManyInterface $relation)
	{
		return true;
	}
	
}
