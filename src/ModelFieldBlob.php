<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Internationalizable\InternationalizableStatusYesOnly;
use PhpExtended\Optionality\OptionalityInterface;

/**
 * ModelFieldBlob class file.
 * 
 * This class represents a blob field in a relational database.
 * 
 * @author Anastaszor
 */
class ModelFieldBlob extends AbstractModelField implements ModelFieldBlobInterface
{
	
	/**
	 * The minimum length, in bytes, of this field.
	 * 
	 * @var integer
	 */
	protected int $_minimumLength;
	
	/**
	 * The maximum length, in bytes, of this field.
	 * 
	 * @var integer
	 */
	protected int $_maximumLength;
	
	/**
	 * The default value of this blob field.
	 * 
	 * @var ?string
	 */
	protected ?string $_defaultValue;
	
	/**
	 * Builds a new ModelFieldBlob with the given field values.
	 * 
	 * @param string $name
	 * @param OptionalityInterface $optionality
	 * @param integer $minlength
	 * @param integer $maxlength
	 * @param ?InternationalizableStatusInterface $status
	 * @param ?string $default
	 * @param ?string $comment
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public function __construct(
		string $name,
		OptionalityInterface $optionality,
		int $minlength,
		int $maxlength,
		?InternationalizableStatusInterface $status = null,
		?string $default = null,
		?string $comment = null
	) {
		if(null === $status)
		{
			$status = new InternationalizableStatusYesOnly();
		}
		parent::__construct($name, $optionality, $status, $comment);
		$this->_minimumLength = $minlength;
		$this->_maximumLength = $maxlength;
		$this->_defaultValue = $default;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'('.((string) $this->_minimumLength).','.((string) $this->_maximumLength).')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldBlobInterface::getMinimumLength()
	 */
	public function getMinimumLength() : int
	{
		return $this->_minimumLength;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldBlobInterface::getMaximumLength()
	 */
	public function getMaximumLength() : int
	{
		return $this->_maximumLength;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldBlobInterface::getDefaultValue()
	 */
	public function getDefaultValue() : ?string
	{
		return $this->_defaultValue;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::mergeWith()
	 */
	public function mergeWith(ModelFieldInterface $field) : ModelFieldInterface
	{
		$minlength = $this->getMinimumLength();
		$maxlength = $this->getMaximumLength();
		
		if($field instanceof ModelFieldBlobInterface)
		{
			$minlength = \min($minlength, $field->getMinimumLength());
			$maxlength = \max($maxlength, $field->getMaximumLength());
		}
		
		return new self(
			$this->getName(),
			$this->getOptionality()->mergeWith($field->getOptionality()),
			$minlength,
			$maxlength,
			$this->getInternationalizedStatus()->mergeWith($field->getInternationalizedStatus()),
			$this->getDefaultValue(),
			$this->getComment(),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::visit()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visit(ModelFieldVisitorInterface $visitor)
	{
		return $visitor->visitBlobField($this);
	}
	
}
