<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Internationalizable\InternationalizableStatusNoOnly;
use PhpExtended\Optionality\OptionalityInterface;

/**
 * ModelFieldSpatial class file.
 * 
 * This class represents a spatial field in a relational database.
 * 
 * @author Anastaszor
 */
class ModelFieldSpatial extends AbstractModelField implements ModelFieldSpatialInterface
{
	
	/**
	 * The default spatial value for this field.
	 * 
	 * @var ?string
	 */
	protected ?string $_defaultValue = null;
	
	/**
	 * Builds a new ModelFieldSpatial with the given field values.
	 * 
	 * @param string $name
	 * @param OptionalityInterface $optionality
	 * @param ?InternationalizableStatusInterface $status
	 * @param ?string $default
	 * @param ?string $comment
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public function __construct(
		string $name,
		OptionalityInterface $optionality,
		?InternationalizableStatusInterface $status = null,
		?string $default = null,
		?string $comment = null
	) {
		if(null === $status)
		{
			$status = new InternationalizableStatusNoOnly();
		}
		parent::__construct($name, $optionality, $status, $comment);
		$this->_defaultValue = $default;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::__toString()
	 */
	public function __toString() : string
	{
		return static::class;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldSpatialInterface::getDefaultValue()
	 */
	public function getDefaultValue() : ?string
	{
		return $this->_defaultValue;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::mergeWith()
	 */
	public function mergeWith(ModelFieldInterface $field) : ModelFieldInterface
	{
		if($field instanceof ModelFieldJsonInterface || $field instanceof ModelFieldBlobInterface)
		{
			return $field->mergeWith($this);
		}
		
		return new self(
			$this->getName(),
			$this->getOptionality()->mergeWith($field->getOptionality()),
			$this->getInternationalizedStatus()->mergeWith($field->getInternationalizedStatus()),
			$this->getDefaultValue(),
			$this->getComment(),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::visit()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visit(ModelFieldVisitorInterface $visitor)
	{
		return $visitor->visitSpatialField($this);
	}
	
}
