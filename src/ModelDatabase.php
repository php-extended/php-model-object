<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * ModelDatabase class file.
 * 
 * This class is a simple implementation of the ModelDatabaseInterface.
 * 
 * @author Anastaszor
 */
class ModelDatabase implements ModelDatabaseInterface
{
	
	/**
	 * Whether this database is enabled.
	 * 
	 * @var boolean
	 */
	protected bool $_isEnabled = true;
	
	/**
	 * The name of the database in the RDBMS.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The schemas attached to this database.
	 * 
	 * @var array<string, ModelSchemaInterface>
	 */
	protected array $_schemas = [];
	
	/**
	 * Builds a new ModelDatabase with the given name and enabled status.
	 * 
	 * @param string $name
	 * @param boolean $enabled
	 */
	public function __construct(string $name, bool $enabled = true)
	{
		$this->_name = $name;
		$this->_isEnabled = $enabled;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelDatabaseInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * Sets whether this data object is enabled.
	 * 
	 * @param boolean $enabled
	 */
	public function setIsEnabled(bool $enabled) : void
	{
		$this->_isEnabled = $enabled;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelDatabaseInterface::isEnabled()
	 */
	public function isEnabled() : bool
	{
		return $this->_isEnabled;
	}
	
	/**
	 * Adds the given schema to the schemas of this database.
	 * 
	 * @param ModelSchemaInterface $schema
	 */
	public function addSchema(ModelSchemaInterface $schema) : void
	{
		if(isset($this->_schemas[$schema->getName()]))
		{
			$schema = $this->_schemas[$schema->getName()]->mergeWith($schema);
		}
		$this->_schemas[$schema->getName()] = $schema;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelDatabaseInterface::mergeWith()
	 */
	public function mergeWith(ModelDatabaseInterface $database) : ModelDatabaseInterface
	{
		$new = new self($this->getName(), $this->isEnabled() || $database->isEnabled());
		
		foreach($this->listModelSchemas() as $schema)
		{
			$new->addSchema($schema);
		}
		
		foreach($database->listModelSchemas() as $schema)
		{
			$new->addSchema($schema);
		}
		
		return $new;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelSchemaProviderInterface::listModelSchemas()
	 */
	public function listModelSchemas() : array
	{
		return \array_values($this->_schemas);
	}
	
}
