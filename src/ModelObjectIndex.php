<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * ModelObjectIndex class file.
 * 
 * This class is a simple implementation of the ModelObjectIndexInterface.
 * 
 * @author Anastaszor
 */
class ModelObjectIndex implements ModelObjectIndexInterface
{
	
	/**
	 * The name of this index.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The name of the fields this index includes.
	 * 
	 * @var array<integer, string>
	 */
	protected array $_fields = [];
	
	/**
	 * Builds a new ModelObjectIndex with the given name and fields.
	 * 
	 * @param string $name
	 * @param array<integer, string> $fields
	 */
	public function __construct(string $name, array $fields = [])
	{
		$this->_name = $name;
		$this->_fields = $fields;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelObjectIndexInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * The fields of this index.
	 * 
	 * @param string $field
	 */
	public function addField(string $field) : void
	{
		$this->_fields[] = $field;
		$this->_fields = \array_unique($this->_fields);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelObjectIndexInterface::getFields()
	 * @return array<integer, string>
	 */
	public function getFields() : array
	{
		return $this->_fields;
	}
	
}
