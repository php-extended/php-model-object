<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Internationalizable\InternationalizableStatusNoOnly;
use PhpExtended\Optionality\OptionalityInterface;

/**
 * ModelFieldJson class file.
 * 
 * This class represents a json field in a relational database.
 * 
 * @author Anastaszor
 */
class ModelFieldJson extends AbstractModelField implements ModelFieldJsonInterface
{
	
	/**
	 * The maximum depths for this field.
	 * 
	 * @var integer
	 */
	protected int $_maximumDepths;
	
	/**
	 * The default json value for this field.
	 * 
	 * @var ?string
	 */
	protected ?string $_defaultValue = null;
	
	/**
	 * Builds a new ModelFieldJson with the given field values.
	 * 
	 * @param string $name
	 * @param OptionalityInterface $optionality
	 * @param integer $maximumDepths
	 * @param ?InternationalizableStatusInterface $status
	 * @param ?string $default
	 * @param ?string $comment
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public function __construct(
		string $name,
		OptionalityInterface $optionality,
		int $maximumDepths = -1,
		?InternationalizableStatusInterface $status = null,
		?string $default = null,
		?string $comment = null
	) {
		if(null === $status)
		{
			$status = new InternationalizableStatusNoOnly();
		}
		parent::__construct($name, $optionality, $status, $comment);
		$this->_maximumDepths = $maximumDepths;
		$this->_defaultValue = $default;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'('.((string) $this->_maximumDepths).')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldJsonInterface::getMaximumDepths()
	 */
	public function getMaximumDepths() : int
	{
		return $this->_maximumDepths;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldJsonInterface::getDefaultValue()
	 */
	public function getDefaultValue() : ?string
	{
		return $this->_defaultValue;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::mergeWith()
	 */
	public function mergeWith(ModelFieldInterface $field) : ModelFieldInterface
	{
		if($field instanceof ModelFieldBlobInterface)
		{
			return $field->mergeWith($this);
		}
		
		$maximumDepths = $this->getMaximumDepths();
		
		if($field instanceof ModelFieldJsonInterface)
		{
			$maximumDepths = \max($maximumDepths, $field->getMaximumDepths());
		}
		
		return new self(
			$this->getName(),
			$this->getOptionality()->mergeWith($field->getOptionality()),
			$maximumDepths,
			$this->getInternationalizedStatus()->mergeWith($field->getInternationalizedStatus()),
			$this->getDefaultValue(),
			$this->getComment(),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::visit()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visit(ModelFieldVisitorInterface $visitor)
	{
		return $visitor->visitJsonField($this);
	}
	
}
