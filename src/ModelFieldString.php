<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use PhpExtended\Charset\CharacterSetInterface;
use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Internationalizable\InternationalizableStatusYesOnly;
use PhpExtended\Optionality\OptionalityInterface;

/**
 * ModelFieldString class file.
 * 
 * This class represents a string field in a relational database.
 * 
 * @author Anastaszor
 */
class ModelFieldString extends AbstractModelField implements ModelFieldStringInterface
{
	
	/**
	 * The charset of this string field.
	 * 
	 * @var CharacterSetInterface
	 */
	protected CharacterSetInterface $_charset;
	
	/**
	 * The minimum length of this string field.
	 * 
	 * @var integer
	 */
	protected int $_minLength;
	
	/**
	 * The maximum length of this string field.
	 * 
	 * @var integer
	 */
	protected int $_maxLength;
	
	/**
	 * The default string value for this field.
	 * 
	 * @var ?string
	 */
	protected ?string $_defaultValue = null;
	
	/**
	 * Builds a new ModelFieldString with the given field values.
	 * 
	 * @param string $name
	 * @param OptionalityInterface $optionality
	 * @param CharacterSetInterface $charset
	 * @param integer $minLength
	 * @param integer $maxLength
	 * @param ?InternationalizableStatusInterface $status
	 * @param ?string $default
	 * @param ?string $comment
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public function __construct(
		string $name,
		OptionalityInterface $optionality,
		CharacterSetInterface $charset,
		int $minLength,
		int $maxLength,
		?InternationalizableStatusInterface $status = null,
		?string $default = null,
		?string $comment = null
	) {
		$this->_charset = $charset;
		$this->_minLength = $minLength;
		$this->_maxLength = $maxLength;
		$this->_defaultValue = $default;
		if(null === $status)
		{
			$status = new InternationalizableStatusYesOnly();
		}
		
		parent::__construct($name, $optionality, $status, $comment);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'['.$this->_charset->getName().']('.((string) $this->_minLength).','.((string) $this->_maxLength).')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldStringInterface::getCharset()
	 */
	public function getCharset() : CharacterSetInterface
	{
		return $this->_charset;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldStringInterface::getMinimumLength()
	 */
	public function getMinimumLength() : int
	{
		return $this->_minLength;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldStringInterface::getMaximumLength()
	 */
	public function getMaximumLength() : int
	{
		return $this->_maxLength;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldStringInterface::getDefaultValue()
	 */
	public function getDefaultValue() : ?string
	{
		return $this->_defaultValue;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::mergeWith()
	 */
	public function mergeWith(ModelFieldInterface $field) : ModelFieldInterface
	{
		if(\is_subclass_of($field, ModelFieldSpatialInterface::class)
			|| \is_subclass_of($field, ModelFieldJsonInterface::class)
			|| \is_subclass_of($field, ModelFieldBlobInterface::class)
		) {
			return $field->mergeWith($this);
		}
		
		$minLength = $this->getMinimumLength();
		$maxLength = $this->getMaximumLength();
		
		if(\is_subclass_of($field, ModelFieldStringInterface::class))
		{
			$minLength = \min($minLength, $field->getMinimumLength());
			$maxLength = \max($maxLength, $field->getMaximumLength());
		}
		
		return new self(
			$this->getName(),
			$this->getOptionality()->mergeWith($field->getOptionality()),
			$this->getCharset(),
			$minLength,
			$maxLength,
			$this->getInternationalizedStatus()->mergeWith($field->getInternationalizedStatus()),
			$this->getDefaultValue(),
			$this->getComment(),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::visit()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visit(ModelFieldVisitorInterface $visitor)
	{
		return $visitor->visitStringField($this);
	}
	
}
