<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Multiplicity\Multiplicity;
use PhpExtended\Optionality\OptionalityInterface;

/**
 * ModelRelationHasMany class file.
 * 
 * This class represents a has many relation from a source model object to a
 * target model object.
 * 
 * @author Anastaszor
 */
class ModelRelationHasMany extends AbstractModelRelation implements ModelRelationHasManyInterface
{
	
	/**
	 * Builds a ModelRelationHasMany with the given name, source and target
	 * objects and their respective internationalizable statuses, as well as
	 * the additional fields this relation may carry.
	 * 
	 * @param string $name
	 * @param OptionalityInterface $optionality
	 * @param ModelObjectInterface $source
	 * @param ModelObjectInterface $target
	 * @param InternationalizableStatusInterface $sourcei
	 * @param InternationalizableStatusInterface $targeti
	 * @param array<integer, ModelFieldInterface> $fields
	 * @param ?string $comment
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public function __construct(
		string $name,
		OptionalityInterface $optionality,
		ModelObjectInterface $source,
		ModelObjectInterface $target,
		InternationalizableStatusInterface $sourcei,
		InternationalizableStatusInterface $targeti,
		array $fields = [],
		?string $comment = null
	) {
		parent::__construct($name, $optionality, $source, $target, Multiplicity::ZERO_MANY, Multiplicity::ZERO_ONE, $sourcei, $targeti, $fields, $comment);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationInterface::mergeWith()
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ElseExpression")
	 */
	public function mergeWith(ModelRelationInterface $relation) : ModelRelationInterface
	{
		if($this->getSource()->getTableName() !== $this->getTarget()->getTableName())
		{
			if($this->getTarget()->getTableName() !== $relation->getSource()->getTableName())
			{
				if($this->getTarget()->getTableName() === $relation->getTarget()->getTableName())
				{
					return $this->mergeWith($relation->reverse());
				}
				
				if($this->getSource()->getTableName() === $relation->getSource()->getTableName())
				{
					return $this->reverse()->mergeWith($relation)->reverse();
				}
				
				if($this->getSource()->getTableName() === $relation->getTarget()->getTableName())
				{
					return $this->reverse()->mergeWith($relation->reverse())->reverse();
				}
			}
		}
		
		if('rel' === $this->getName() || $this->getName() === $relation->getName())
		{
			$name = $relation->getName();
		}
		elseif('rel' === $relation->getName())
		{
			$name = $this->getName();
		}
		else
		{
			$name = $this->getName().'_'.$relation->getName();
		}
		
		$mrgOptnlty = $this->getOptionality()->mergeWith($relation->getOptionality());
		$mrgSrcI18n = $this->getSourceInternationalizable()->mergeWith($relation->getSourceInternationalizable());
		$mrgTgtI18n = $this->getTargetInternationalizable()->mergeWith($relation->getTargetInternationalizable());
		$mrgFields = [];
		
		foreach($this->getFields() as $field)
		{
			$mrgFields[] = $field;
		}
		
		foreach($relation->getFields() as $field)
		{
			$mrgFields[] = $field;
		}
		
		$mrgComment = \trim(((string) $this->getComment()).' '.((string) $relation->getComment()));
		
		// Has-Many may be chained
		if($relation->getSourceMultiplicity()->isMultiple() && !$relation->getTargetMultiplicity()->isMultiple())
		{
			return new self(
				$name,
				$mrgOptnlty,
				$this->getSource(),
				$relation->getTarget(),
				$mrgSrcI18n,
				$mrgTgtI18n,
				$mrgFields,
				$mrgComment,
			);
		}
		
		return new ModelRelationManyMany(
			$name,
			$mrgOptnlty,
			$this->getSource(),
			$relation->getTarget(),
			$mrgSrcI18n,
			$mrgTgtI18n,
			$mrgFields,
			$mrgComment,
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationInterface::visit()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visit(ModelRelationVisitorInterface $visitor)
	{
		return $visitor->visitHasManyRelation($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationHasManyInterface::reverse()
	 */
	public function reverse() : ModelRelationInterface
	{
		return new ModelRelationBelongsTo(
			$this->getName(),
			$this->getOptionality(),
			$this->getTarget(),
			$this->getSource(),
			$this->getTargetInternationalizable(),
			$this->getSourceInternationalizable(),
			$this->getFields(),
			$this->getComment(),
		);
	}
	
}
