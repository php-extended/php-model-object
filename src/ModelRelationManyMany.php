<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Multiplicity\Multiplicity;
use PhpExtended\Optionality\OptionalityInterface;

/**
 * ModelRelationManyMany class file.
 * 
 * This class represents a many many relation from a source model to a target
 * model object.
 * 
 * @author Anastaszor
 */
class ModelRelationManyMany extends AbstractModelRelation implements ModelRelationManyManyInterface
{
	
	/**
	 * Builds a ModelRelationManyMany with the given name, source and target
	 * objects and their respective internationalizable statuses, as well as
	 * the additional fields this relation may carry.
	 * 
	 * @param string $name
	 * @param OptionalityInterface $optionalty
	 * @param ModelObjectInterface $source
	 * @param ModelObjectInterface $target
	 * @param InternationalizableStatusInterface $sourcei
	 * @param InternationalizableStatusInterface $targeti
	 * @param array<integer, ModelFieldInterface> $fields
	 * @param ?string $comment
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public function __construct(
		string $name,
		OptionalityInterface $optionalty,
		ModelObjectInterface $source,
		ModelObjectInterface $target,
		InternationalizableStatusInterface $sourcei,
		InternationalizableStatusInterface $targeti,
		array $fields = [],
		?string $comment = null
	) {
		parent::__construct($name, $optionalty, $source, $target, Multiplicity::ZERO_MANY, Multiplicity::ZERO_MANY, $sourcei, $targeti, $fields, $comment);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationInterface::mergeWith()
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.ElseExpression")
	 */
	public function mergeWith(ModelRelationInterface $relation) : ModelRelationInterface
	{
		if($this->getSource()->getTableName() !== $this->getTarget()->getTableName())
		{
			if($this->getTarget()->getTableName() !== $relation->getSource()->getTableName())
			{
				if($this->getTarget()->getTableName() === $relation->getTarget()->getTableName())
				{
					return $this->mergeWith($relation->reverse());
				}
				
				if($this->getSource()->getTableName() === $relation->getSource()->getTableName())
				{
					return $this->reverse()->mergeWith($relation)->reverse();
				}
				
				if($this->getSource()->getTableName() === $relation->getTarget()->getTableName())
				{
					return $this->reverse()->mergeWith($relation->reverse())->reverse();
				}
			}
		}
		
		if('rel' === $this->getName() || $this->getName() === $relation->getName())
		{
			$name = $relation->getName();
		}
		elseif('rel' === $relation->getName())
		{
			$name = $this->getName();
		}
		else
		{
			$name = $this->getName().'_'.$relation->getName();
		}
		
		$mrgOptnlty = $this->getOptionality()->mergeWith($relation->getOptionality());
		$mrgSrcI18n = $this->getSourceInternationalizable()->mergeWith($relation->getSourceInternationalizable());
		$mrgTgtI18n = $this->getTargetInternationalizable()->mergeWith($relation->getTargetInternationalizable());
		$mrgFields = [];
		
		foreach($this->getFields() as $field)
		{
			$mrgFields[] = $field;
		}
		
		foreach($relation->getFields() as $field)
		{
			$mrgFields[] = $field;
		}
		
		$mrgComment = \trim(((string) $this->getComment()).' '.((string) $relation->getComment()));
		
		return new self(
			$name,
			$mrgOptnlty,
			$this->getSource(),
			$relation->getTarget(),
			$mrgSrcI18n,
			$mrgTgtI18n,
			$mrgFields,
			$mrgComment,
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationInterface::visit()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visit(ModelRelationVisitorInterface $visitor)
	{
		return $visitor->visitManyManyRelation($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationManyManyInterface::reverse()
	 */
	public function reverse() : ModelRelationInterface
	{
		return new self(
			$this->getName(),
			$this->getOptionality(),
			$this->getTarget(),
			$this->getSource(),
			$this->getTargetInternationalizable(),
			$this->getSourceInternationalizable(),
			$this->getFields(),
			$this->getComment(),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationManyManyInterface::getRelationalTableName()
	 */
	public function getRelationalTableName() : string
	{
		return $this->getSource()->getTableName().'_rel_'.$this->getTarget()->getTableName();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationManyManyInterface::getI18nRelationalTableName()
	 */
	public function getI18nRelationalTableName() : string
	{
		return $this->getSource()->getI18nTableName().'_rel_'.$this->getSource()->getI18nTableName();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationManyManyInterface::getRelationalTableComment()
	 */
	public function getRelationalTableComment() : string
	{
		return 'Table to store all the '.$this->getTarget()->getTableName().' records'
			.' in relation with the '.$this->getSource()->getTableName().' records';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationManyManyInterface::getI18nRelationalTableComment()
	 */
	public function getI18nRelationalTableComment() : string
	{
		return 'Table to store all the '.$this->getTarget()->getI18nTableName().' records'
			.' in relation with the '.$this->getSource()->getI18nTableName().' records';
	}
	
}
