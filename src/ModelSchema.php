<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * ModelSchema class file.
 * 
 * This class is a simple implementation of the ModelSchemaInterface.
 * 
 * @author Anastaszor
 */
class ModelSchema implements ModelSchemaInterface
{
	
	/**
	 * Whether this schema is enabled.
	 * 
	 * @var boolean
	 */
	protected bool $_isEnabled = true;
	
	/**
	 * Whether this schema is transparent.
	 * 
	 * @var boolean
	 */
	protected bool $_isTransparent = false;
	
	/**
	 * The class name of this schema.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The schemas attached to this schema.
	 * 
	 * @var array<string, ModelObjectInterface>
	 */
	protected array $_objects = [];
	
	/**
	 * Builds a new ModelSchema with the given arguments.
	 * 
	 * @param string $name
	 * @param boolean $enabled
	 * @param boolean $transparent
	 */
	public function __construct(string $name, bool $enabled = true, bool $transparent = false)
	{
		$this->_name = $name;
		$this->setIsEnabled($enabled);
		$this->setIsTransparent($transparent);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelDatabaseInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * Sets whether this data object is enabled.
	 * 
	 * @param boolean $enabled
	 */
	public function setIsEnabled(bool $enabled) : void
	{
		$this->_isEnabled = $enabled;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelDatabaseInterface::isEnabled()
	 */
	public function isEnabled() : bool
	{
		return $this->_isEnabled;
	}
	
	/**
	 * Sets whether this data object is transparent.
	 * 
	 * @param boolean $transparent
	 */
	public function setIsTransparent(bool $transparent) : void
	{
		$this->_isTransparent = $transparent;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelDatabaseInterface::isTransparent()
	 */
	public function isTransparent() : bool
	{
		return $this->_isTransparent;
	}
	
	/**
	 * Adds the given object to the objects of this schema.
	 * 
	 * @param ModelObjectInterface $object
	 */
	public function addObject(ModelObjectInterface $object) : ModelSchemaInterface
	{
		if(isset($this->_objects[$object->getTableName()]))
		{
			$object = $this->_objects[$object->getTableName()]->mergeWith($object);
		}
		
		$this->_objects[$object->getTableName()] = $object;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelSchemaInterface::mergeWith()
	 */
	public function mergeWith(ModelSchemaInterface $schema) : ModelSchemaInterface
	{
		$new = new self(
			$this->getName(),
			$this->isEnabled() || $schema->isEnabled(),
			$this->isTransparent() || $schema->isTransparent(),
		);
		
		foreach($this->listModelObjects() as $object)
		{
			$new->addObject($object);
		}
		
		foreach($schema->listModelObjects() as $object)
		{
			$new->addObject($object);
		}
		
		return $new;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelObjectProviderInterface::listModelObjects()
	 */
	public function listModelObjects() : array
	{
		return \array_values($this->_objects);
	}
	
}
