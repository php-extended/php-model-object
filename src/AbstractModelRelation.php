<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Multiplicity\MultiplicityInterface;
use PhpExtended\Optionality\OptionalityInterface;

/**
 * AbstractModelRelation class file.
 * 
 * This class collects all the informations that are the same for all the
 * relation types.
 * 
 * @author Anastaszor
 */
abstract class AbstractModelRelation implements ModelRelationInterface
{
	
	/**
	 * The name of the relation.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The optionality of this field.
	 * 
	 * @var OptionalityInterface
	 */
	protected OptionalityInterface $_optionality;
	
	/**
	 * The source object.
	 * 
	 * @var ModelObjectInterface
	 */
	protected ModelObjectInterface $_source;
	
	/**
	 * The multiplicity of the source.
	 * 
	 * @var MultiplicityInterface
	 */
	protected MultiplicityInterface $_sourceMultiplicity;
	
	/**
	 * @var InternationalizableStatusInterface
	 */
	protected InternationalizableStatusInterface $_sourceInterbility;
	
	/**
	 * The target object.
	 * 
	 * @var ModelObjectInterface
	 */
	protected ModelObjectInterface $_target;
	
	/**
	 * The multiplicity of the target.
	 * 
	 * @var MultiplicityInterface
	 */
	protected MultiplicityInterface $_targetMultiplicity;
	
	/**
	 * @var InternationalizableStatusInterface
	 */
	protected InternationalizableStatusInterface $_targetInterbility;
	
	/**
	 * The fields of this relation.
	 * 
	 * @var array<integer, ModelFieldInterface>
	 */
	protected array $_fields = [];
	
	/**
	 * The comment for this relation.
	 * 
	 * @var ?string
	 */
	protected ?string $_comment = null;
	
	/**
	 * Builds a new AbstractModelRelation with the given name, source, target,
	 * their respective multiplicities and internationalizable statuses, and
	 * the additional fields that this relation may carry.
	 * 
	 * @param string $name
	 * @param OptionalityInterface $optionality
	 * @param ModelObjectInterface $source
	 * @param ModelObjectInterface $target
	 * @param MultiplicityInterface $sourcem
	 * @param MultiplicityInterface $targetm
	 * @param InternationalizableStatusInterface $sourcei
	 * @param InternationalizableStatusInterface $targeti
	 * @param array<integer, ModelFieldInterface> $fields
	 * @param ?string $comment
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(
		string $name,
		OptionalityInterface $optionality,
		ModelObjectInterface $source,
		ModelObjectInterface $target,
		MultiplicityInterface $sourcem,
		MultiplicityInterface $targetm,
		InternationalizableStatusInterface $sourcei,
		InternationalizableStatusInterface $targeti,
		array $fields = [],
		?string $comment = null
	) {
		$this->_name = $name;
		$this->_optionality = $optionality;
		$this->_source = $source;
		$this->_sourceMultiplicity = $sourcem;
		$this->_sourceInterbility = $sourcei;
		$this->_target = $target;
		$this->_targetMultiplicity = $targetm;
		$this->_targetInterbility = $targeti;
		$this->_fields = $fields;
		$this->_comment = $comment;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.' ('.$this->_name.')'.($this->_optionality->isNullAllowed() ? ' [N]' : '')
			.' from '.$this->_source->getTableName().' (i18n : '.\get_class($this->_sourceInterbility).') ['.\get_class($this->_sourceMultiplicity).']'
			.' to '.$this->_target->getTableName().' (i18n : '.\get_class($this->_targetInterbility).') ['.\get_class($this->_targetMultiplicity).']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelObjectInterface::getComment()
	 * @return null|string
	 */
	public function getComment()
	{
		return $this->_comment;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationInterface::getOptionality()
	 */
	public function getOptionality() : OptionalityInterface
	{
		return $this->_optionality;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationInterface::getSource()
	 */
	public function getSource() : ModelObjectInterface
	{
		return $this->_source;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationInterface::getTarget()
	 */
	public function getTarget() : ModelObjectInterface
	{
		return $this->_target;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationInterface::getSourceMultiplicity()
	 */
	public function getSourceMultiplicity() : MultiplicityInterface
	{
		return $this->_sourceMultiplicity;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationInterface::getTargetMultiplicity()
	 */
	public function getTargetMultiplicity() : MultiplicityInterface
	{
		return $this->_targetMultiplicity;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationInterface::getSourceInternationalizable()
	 */
	public function getSourceInternationalizable() : InternationalizableStatusInterface
	{
		return $this->_sourceInterbility;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationInterface::getTargetInternationalizable()
	 */
	public function getTargetInternationalizable() : InternationalizableStatusInterface
	{
		return $this->_targetInterbility;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelRelationInterface::getFields()
	 * @return array<integer, ModelFieldInterface>
	 */
	public function getFields() : array
	{
		return $this->_fields;
	}
	
}
