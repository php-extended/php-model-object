<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * ModelFieldVisitor class file.
 * 
 * This class is a noop field visitor to be extended.
 * 
 * @author Anastaszor
 */
class ModelFieldVisitor implements ModelFieldVisitorInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldVisitorInterface::visitBlobField()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitBlobField(ModelFieldBlobInterface $blobField)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldVisitorInterface::visitBooleanField()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitBooleanField(ModelFieldBooleanInterface $booleanField)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldVisitorInterface::visitDateTimeField()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitDateTimeField(ModelFieldDateTimeInterface $dateTimeField)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldVisitorInterface::visitFloatField()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFloatField(ModelFieldFloatInterface $floatField)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldVisitorInterface::visitIntegerField()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitIntegerField(ModelFieldIntegerInterface $integerField)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldVisitorInterface::visitJsonField()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitJsonField(ModelFieldJsonInterface $jsonField)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldVisitorInterface::visitSpatialField()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitSpatialField(ModelFieldSpatialInterface $spatialField)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldVisitorInterface::visitStringField()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitStringField(ModelFieldStringInterface $stringField)
	{
		return null;
	}
	
}
