<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Optionality\OptionalityInterface;

/**
 * AbstractModelField class file.
 * 
 * This class collects all the informations that are the same for all the
 * field types.
 * 
 * @author Anastaszor
 */
abstract class AbstractModelField implements ModelFieldInterface
{
	
	/**
	 * The name of the field.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The optionality of this field.
	 * 
	 * @var OptionalityInterface
	 */
	protected OptionalityInterface $_optionality;
	
	/**
	 * The internationalizable status of this field.
	 * 
	 * @var InternationalizableStatusInterface
	 */
	protected InternationalizableStatusInterface $_interzableStatus;
	
	/**
	 * The comment about this field.
	 * 
	 * @var ?string
	 */
	protected ?string $_comment = null;
	
	/**
	 * Builds a new AbstractModelField with the given name and i18n status.
	 * 
	 * @param string $name
	 * @param OptionalityInterface $optionality
	 * @param InternationalizableStatusInterface $status
	 * @param string $comment
	 */
	public function __construct(
		string $name,
		OptionalityInterface $optionality,
		InternationalizableStatusInterface $status,
		?string $comment = null
	) {
		$this->_name = $name;
		$this->_optionality = $optionality;
		$this->_interzableStatus = $status;
		$this->_comment = $comment;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::getOptionality()
	 */
	public function getOptionality() : OptionalityInterface
	{
		return $this->_optionality;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::getInternationalizedStatus()
	 */
	public function getInternationalizedStatus() : InternationalizableStatusInterface
	{
		return $this->_interzableStatus;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\ModelFieldInterface::getComment()
	 */
	public function getComment() : ?string
	{
		return $this->_comment;
	}
	
}
