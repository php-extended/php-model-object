<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Model\ModelFieldBlob;
use PhpExtended\Optionality\OptionalityInterface;
use PHPUnit\Framework\TestCase;

/**
 * ModelFieldBlobTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Model\ModelFieldBlob
 *
 * @internal
 *
 * @small
 */
class ModelFieldBlobTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ModelFieldBlob
	 */
	protected ModelFieldBlob $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'(0,12)', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ModelFieldBlob(
			'name',
			$this->getMockForAbstractClass(OptionalityInterface::class),
			0,
			12,
			$this->getMockForAbstractClass(InternationalizableStatusInterface::class),
			'default',
			'comment',
		);
	}
	
}
