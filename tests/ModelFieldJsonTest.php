<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Model\ModelFieldJson;
use PhpExtended\Optionality\OptionalityInterface;
use PHPUnit\Framework\TestCase;

/**
 * ModelFieldJsonTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Model\ModelFieldJson
 *
 * @internal
 *
 * @small
 */
class ModelFieldJsonTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ModelFieldJson
	 */
	protected ModelFieldJson $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'(1)', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ModelFieldJson(
			'name',
			$this->getMockForAbstractClass(OptionalityInterface::class),
			1,
			$this->getMockForAbstractClass(InternationalizableStatusInterface::class),
			'default',
			'comment',
		);
	}
	
}
