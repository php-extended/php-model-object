<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Internationalizable\InternationalizableStatusNoOnly;
use PhpExtended\Model\ModelObjectInterface;
use PhpExtended\Model\ModelRelationBelongsTo;
use PhpExtended\Optionality\Optionality;
use PHPUnit\Framework\TestCase;

/**
 * ModelRelationBelongsToTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Model\ModelRelationBelongsTo
 *
 * @internal
 *
 * @small
 */
class ModelRelationBelongsToTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ModelRelationBelongsTo
	 */
	protected ModelRelationBelongsTo $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).' (name) from  (i18n : PhpExtended\\Internationalizable\\InternationalizableStatusNoOnly) [PhpExtended\\Multiplicity\\Multiplicity] to  (i18n : PhpExtended\\Internationalizable\\InternationalizableStatusNoOnly) [PhpExtended\\Multiplicity\\Multiplicity]', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ModelRelationBelongsTo(
			'name',
			Optionality::NO_NULL_EMPTY_ALLOWED,
			$this->getMockForAbstractClass(ModelObjectInterface::class),
			$this->getMockForAbstractClass(ModelObjectInterface::class),
			new InternationalizableStatusNoOnly(),
			new InternationalizableStatusNoOnly(),
			[],
			'comment',
		);
	}
	
}
