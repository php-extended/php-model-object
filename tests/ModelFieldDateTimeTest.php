<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DateTimeFormat\DateTimeFormatInterface;
use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Model\ModelFieldDateTime;
use PhpExtended\Optionality\OptionalityInterface;
use PHPUnit\Framework\TestCase;

/**
 * ModelFieldDateTimeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Model\ModelFieldDateTime
 *
 * @internal
 *
 * @small
 */
class ModelFieldDateTimeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ModelFieldDateTime
	 */
	protected ModelFieldDateTime $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'[]', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ModelFieldDateTime(
			'name',
			$this->getMockForAbstractClass(OptionalityInterface::class),
			$this->getMockForAbstractClass(DateTimeFormatInterface::class),
			$this->getMockForAbstractClass(InternationalizableStatusInterface::class),
			true,
			true,
			new DateTimeImmutable(),
			'comment',
		);
	}
	
}
