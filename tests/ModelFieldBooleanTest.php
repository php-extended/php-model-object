<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Model\ModelFieldBoolean;
use PHPUnit\Framework\TestCase;

/**
 * ModelFieldBooleanTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Model\ModelFieldBoolean
 *
 * @internal
 *
 * @small
 */
class ModelFieldBooleanTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ModelFieldBoolean
	 */
	protected ModelFieldBoolean $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ModelFieldBoolean(
			'name',
			$this->getMockForAbstractClass(InternationalizableStatusInterface::class),
			false,
			'comment',
		);
	}
	
}
