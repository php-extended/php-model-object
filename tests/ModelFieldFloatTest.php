<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Model\ModelFieldFloat;
use PhpExtended\Optionality\OptionalityInterface;
use PHPUnit\Framework\TestCase;

/**
 * ModelFieldFloatTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Model\ModelFieldFloat
 *
 * @internal
 *
 * @small
 */
class ModelFieldFloatTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ModelFieldFloat
	 */
	protected ModelFieldFloat $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'(1,0)', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ModelFieldFloat(
			'name',
			$this->getMockForAbstractClass(OptionalityInterface::class),
			1,
			23,
			$this->getMockForAbstractClass(InternationalizableStatusInterface::class),
			0.3,
			'comment',
		);
	}
	
}
