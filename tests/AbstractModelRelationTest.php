<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Internationalizable\InternationalizableStatusNoOnly;
use PhpExtended\Model\AbstractModelRelation;
use PhpExtended\Model\ModelObjectInterface;
use PhpExtended\Multiplicity\Multiplicity;
use PhpExtended\Optionality\Optionality;
use PHPUnit\Framework\TestCase;

/**
 * AbstractModelRelationTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Model\AbstractModelRelation
 *
 * @internal
 *
 * @small
 */
class AbstractModelRelationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var AbstractModelRelation
	 */
	protected AbstractModelRelation $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).' (name) from  (i18n : PhpExtended\\Internationalizable\\InternationalizableStatusNoOnly) [PhpExtended\\Multiplicity\\Multiplicity] to  (i18n : PhpExtended\\Internationalizable\\InternationalizableStatusNoOnly) [PhpExtended\\Multiplicity\\Multiplicity]', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = $this->getMockForAbstractClass(AbstractModelRelation::class, [
			'name',
			Optionality::NO_NULL_EMPTY_ALLOWED,
			$this->getMockForAbstractClass(ModelObjectInterface::class),
			$this->getMockForAbstractClass(ModelObjectInterface::class),
			Multiplicity::ONE_MANY,
			Multiplicity::ONE_MANY,
			new InternationalizableStatusNoOnly(),
			new InternationalizableStatusNoOnly(),
			[],
			'comment',
		]);
	}
	
}
