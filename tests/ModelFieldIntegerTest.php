<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Model\IntegerCapacityInterface;
use PhpExtended\Model\ModelFieldInteger;
use PhpExtended\Optionality\OptionalityInterface;
use PHPUnit\Framework\TestCase;

/**
 * ModelFieldIntegerTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Model\ModelFieldInteger
 *
 * @internal
 *
 * @small
 */
class ModelFieldIntegerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ModelFieldInteger
	 */
	protected ModelFieldInteger $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'(0)', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ModelFieldInteger(
			'name',
			$this->getMockForAbstractClass(OptionalityInterface::class),
			true,
			$this->getMockForAbstractClass(IntegerCapacityInterface::class),
			$this->getMockForAbstractClass(InternationalizableStatusInterface::class),
			42,
			'comment',
		);
	}
	
}
