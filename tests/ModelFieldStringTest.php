<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\CharacterSetInterface;
use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Model\ModelFieldString;
use PhpExtended\Optionality\OptionalityInterface;
use PHPUnit\Framework\TestCase;

/**
 * ModelFieldStringTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Model\ModelFieldString
 *
 * @internal
 *
 * @small
 */
class ModelFieldStringTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ModelFieldString
	 */
	protected ModelFieldString $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'[](1,12)', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ModelFieldString(
			'name',
			$this->getMockForAbstractClass(OptionalityInterface::class),
			$this->getMockForAbstractClass(CharacterSetInterface::class),
			1,
			12,
			$this->getMockForAbstractClass(InternationalizableStatusInterface::class),
			'default',
			'comment',
		);
	}
	
}
